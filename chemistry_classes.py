# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 23:05:22 2014
chemistry_classes: Atom, Molecule, Compound
@author: benito
"""
from element import elementList as atom_number_to_Symbol
# atom_number_to_Symbol = ['H', 'C', 'O', 'Mg', 'K', 'Cl', 'Hg', 'N', 'P']
class Atom:
	def __init__(self, atom_number=None, x=0.0, y=0.0, z=0.0):
		self.atom_number = atom_number
		self.position = (x,y,z)

	def symbol(self):   # a class method
		return atom_number_to_Symbol[self.atom_number]

	def __repr__(self): # overloads printing
		return '[%2s]:: N=%3d p=[%10.4f, %10.4f, %10.4f]' % \
			(self.symbol(), self.atom_number, self.position[0],
			 self.position[1],self.position[2])

at = Atom(6,0.0,1.0,2.0)
print at
#-> 6  0.0000  1.0000 2.0000

print at.symbol()

#%% --------------------------------------------------------------------

class Molecule:
	def __init__(self, name='Generic'):
		self.name = name
		self.atomlist = []
	def addatom(self, atom):
		self.atomlist.append(atom)
	def __repr__(self):
		molecule_name = 'This is a molecule named %s\n' % self.name
		molecule_name = molecule_name+'It has %d atoms\n' % len(self.atomlist)
		for atom in self.atomlist:
			molecule_name = molecule_name + repr(atom)  + '\n'
		return molecule_name


class Molecula(list):
    def __init__(self,name='Generic'):
        self.name = name
    def addatom(self, atom):
        self.append(atom)
    def __repr__(self):
        string = 'This is a molecule named %s\n' % self.name
        string += 'It has %d atoms\n' % len(self)
        for atom in self:
            string += repr(atom) + '\n'
        return string

print at

mol = Molecula('Water')
at = Atom(7,0.,0.,0.)
mol.append(at)
mol.addatom(Atom(0,0.,0.,1.))
mol.append(Atom(0,0.,1.,0.))
print mol

#6  0.0000  1.0000 2.0000

#%% --------------------------------------------------------------------

ss = at.symbol()
print ss
print mol[2].symbol()

#%% --------------------------------------------------------------------

mol = Molecule('Agua')
at = Atom(7,0.,0.,0.)
mol.addatom(at)
mol.addatom(Atom(0,0.,0.,1.))
mol.addatom(Atom(0,0.,1.,0.))
print ' ... H2O'
print mol

#%% --------------------------------------------------------------------

class QM_molecule(Molecule):
    def addbasis(self):
        self.basis = []
        for atom in self.atomlist:
            self.basis = 0#add_bf(atom, self.basis)

class QN_molecule(Molecule):
    def __repr__(self):
        value = 'QM Rules!\n'
        for atom in self.atomlist:
            value += repr(atom) + '\n'
            return value

class QP_molecule(Molecule):
	def __init__(self, name="Generic", basis="6-31G**"):
		self.basis = basis
		Molecule.__init__(self, name)
	def __getitem__(self,index):
           """so you can reference the atom as an element
           in a list"""
           return self.atomlist[index]
	def __setitem__(self, index, value):
		self.atomlist[index] = value
	def __len__(self):
		return len(mol)
	def __getslice__(self, low, high):
		return self.atomlist[low:high]
	def __add__(self, other):
		self.addatom(other)
		return self.atomlist

class E_Atom:
	def __init__(self, atom_number=0, x=0.0, y=0.0, z=0.0):
		self.atom_number = atom_number
		self.__position = (x,y,z) #<- position is private
	def symbol(self):   # a class method
		return atom_number_to_Symbol[self.atom_number]
	def getposition(self):
		return self.__position
	def setposition(self,x,y,z):
		self.__position = (x,y,z) #<- typecheck first!
	def translate(self, x, y, z):
		x0, y0, z0 = self.__position
		self.__position = (x0+x,y0+y,z0+z)
	def __repr__(self): # overloads printing
		return '[%2s]:: N=%3d, p=[%10.4f, %10.4f, %10.4f]' % \
			(self.symbol(), self.atom_number, self.__position[0],
			 self.__position[1],self.__position[2])

at = E_Atom(1,1.0,3.0,2.0)
print at
print at.getposition()
at.translate(1,-1,2)
print at.getposition()

#%% --------------------------------------------------------------------

print '\ncreating QM_molecule\n'

mol = QP_molecule('Soda') #defined as before
at  = E_Atom(16,0.,0.,0.)
at3 = E_Atom(24,2.,3.,4.5)
print at
mol.addatom(at)
mol.addatom(E_Atom(19,0.,0.,1.))
mol.addatom(E_Atom(23,0.,1.,0.))
print mol

#%% --------------------------------------------------------------------

print 'replacing molecule 2, mol[2] = at3 = E_Atom(4,2.,3.,4.5)'
mol[2] = at3
for atom in mol:        #use like a list!
    print atom
print ' ... after replace'
print 'mol[2] is ::>'
print mol[2]

#%% --------------------------------------------------------------------

print 'translating mol[0] by (1.,2.,3.)'
mol[0].translate(1.,2.,3.)
print mol

#%% --------------------------------------------------------------------

print 'adding atom via mol + at3'
mol = mol + at3
for atom in mol:        #use like a list!
    print atom

#%% --------------------------------------------------------------------

print 'the number of atoms in the molecule is ' + str(len(mol))
mol2 = mol[1:3]
print 'slice of mol, mol2 = mol[1:3]'
for atom in mol2:        #use like a list!
    print atom

print'\n\n   ... done!'

#%% --------------------------------------------------------------------


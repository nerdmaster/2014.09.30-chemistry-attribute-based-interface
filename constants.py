# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 09:49:17 2014

@author: benito
"""

"""
This module contains a number of physical constants to be made available
throughout ChemPy. ChemPy uses SI units throughout; accordingly, all of the
constants in this module are stored in combinations of meters, seconds,
kilograms, moles, etc.

The constants available are listed below. All values were taken from
`NIST <http://physics.nist.gov/cuu/Constants/index.html>`_

"""

import numpy as math

################################################################################

#: The Avogadro constant
Na = 6.02214179e23

#: The Boltzmann constant
kB = 1.3806504e-23

#: The gas law constant
R = 8.314472

#: The Planck constant
h = 6.62606896e-34

#: The speed of light in a vacuum
c = 299792458

#: pi
pi = float(math.pi)


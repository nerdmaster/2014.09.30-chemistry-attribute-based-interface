# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 10:47:51 2014
Test element.py module
@author: benito
"""

import element as ee

for element in ee.elementList:
    print "Element(%3s, '%2s', '%13s', %12s)" % \
    (element.number, element.symbol, element.name, element.mass)

print ' ... done!'
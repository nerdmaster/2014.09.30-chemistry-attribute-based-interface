# README #

This repository contains sample code in Python to show some basic OOP concepts
Originally created for the class ME-369P - Application Programming for Engineers
taught Fall 2014 at **The University of Texas at Austin**

### What is this repository for? ###

* Quick summary
  Again code that shows basic classes, inheritance, magic methods, how to make a class behave like a list or function, etc.
* Version 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact